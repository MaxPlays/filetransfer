package main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.Socket;

import javax.swing.JOptionPane;

public class sender {

	public static void sendFile(final File file, final String ip){
		
		
	Thread t = new Thread(new Runnable() {
		
		@Override
		public void run() {
			try {
				
				mainframe.push.setEnabled(false);
				mainframe.bar.setVisible(true);
				
				System.out.println("Sender: Attempting to connect to " + ip + "...");
				
				Socket s = new Socket(ip, 4816);
				DataOutputStream dos = new DataOutputStream(s.getOutputStream());
				DataInputStream dis = new DataInputStream(s.getInputStream());
				
				System.out.println("Sender: Connected to " + ip + " at port 4816");
				
				FileInputStream fis = new FileInputStream(file);
				String filename = file.getName();
				
				if(!dis.readUTF().equals("end")){
					
					System.out.println("Sender: Sending file " + filename + " to " + ip);
					dos.writeUTF(filename);
					dos.flush();
					
					long size = file.length();
					byte[] b = new byte[1024];
					
					int read;
					
					dos.writeUTF(Long.toString(size));
					dos.flush();
					
					System.out.println("Sender: File size: " + size);
					System.out.println("Sender: Buffer size: " + s.getReceiveBufferSize());
					
					long total = 0;
					
					while((read = fis.read(b)) != -1){
						dos.write(b, 0, read);
						dos.flush();
						total += read;
						run.frame.setBar((int)Math.round(((double)total / (double)size) * 100f));
					}
					fis.close();
					dos.flush();
					System.out.println("Sender: Pushed " + size + " bytes to upstream");
					dos.writeUTF("end");
					System.out.println("Sender: Done");
					dos.flush();
					
					dis.close();
					dos.close();
					s.close();
					
					JOptionPane.showMessageDialog(run.frame, "File " + filename + " was sent successfully");
					mainframe.bar.setValue(0);
					mainframe.push.setEnabled(true);
					mainframe.bar.setVisible(false);
					
				}
				
				return;
			} catch (IOException e) {
				JOptionPane.showMessageDialog(run.frame, "Connection timed out", "Connection Error", 0);
				mainframe.push.setEnabled(true);
				mainframe.bar.setVisible(false);
				e.printStackTrace();
			}
			
		}
	});
	t.start();
		
		
	}
	
}
