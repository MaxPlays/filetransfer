package main;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import java.awt.Color;

/**
* Created by Max_Plays on 31. J�n. 2017
*/
public class receive extends JFrame {

	private static final long serialVersionUID = 7059984634848842236L;
	private JPanel contentPane;
	private JProgressBar bar;
	
	public receive(String i, String f, long s) {
		setTitle("Transmission");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 355, 139);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(run.frame);
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		JLabel lblNewLabel = new JLabel("Transmission from: ");
		lblNewLabel.setBounds(5, 11, 102, 14);
		contentPane.add(lblNewLabel);
		
		JLabel ip = new JLabel(i);
		ip.setBounds(102, 11, 76, 14);
		contentPane.add(ip);
		
		JLabel file = new JLabel(f);
		file.setBounds(36, 36, 308, 14);
		contentPane.add(file);
		
		JLabel lblFile = new JLabel("File:");
		lblFile.setBounds(5, 36, 46, 14);
		contentPane.add(lblFile);
		
		JLabel lblSize = new JLabel("Size:");
		lblSize.setBounds(5, 56, 46, 14);
		contentPane.add(lblSize);
		
		JLabel size = new JLabel(toRead(s, false));
		size.setBounds(36, 56, 46, 14);
		contentPane.add(size);
		
		bar = new JProgressBar();
		bar.setForeground(new Color(0, 128, 0));
		bar.setStringPainted(true);
		bar.setBounds(5, 81, 339, 25);
		contentPane.add(bar);
		
		setVisible(true);
	}
	public static String toRead(long bytes, boolean si) {
	    int unit = si ? 1000 : 1024;
	    if (bytes < unit) return bytes + " B";
	    int exp = (int) (Math.log(bytes) / Math.log(unit));
	    String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp-1) + (si ? "" : "");
	    return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}
	public void setBar(int value){
		bar.setValue(value);
	}
}
