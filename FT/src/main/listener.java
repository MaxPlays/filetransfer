package main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import javax.swing.JOptionPane;

public class listener {

	private ServerSocket sock;
	private Thread t;
	
	public listener(){
		try {
			sock = new ServerSocket(4816);
			listen();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void listen(){
		
		t = new Thread(new Runnable() {
			
			public void run() {
				
				Socket s;
				receive r = null;
				try {
					while((s = sock.accept()) != null){
						
						System.out.println("Recipient: Incoming transmission from " + s.getInetAddress().getHostAddress());						
						DataInputStream dis = new DataInputStream(s.getInputStream());
						DataOutputStream dos = new DataOutputStream(s.getOutputStream());
						
						String ip = s.getInetAddress().getHostAddress();
						
						dos.writeUTF("ready");
						dos.flush();
						
						String filename = dis.readUTF();
						long size = Long.valueOf(dis.readUTF());
						System.out.println("Recipient: Receiving file " + filename + " (" + size + " bytes)");
						
						r = new receive(ip, filename, size);
						
						byte[] b = new byte[1024];
						System.out.println("Recipient: Starting transmission...");
						FileOutputStream fos = new FileOutputStream(new File(filename), true);
						
						int read;
						long total = 0;
						
		                while ((read = dis.read(b, 0, b.length)) != -1 ) {
		                    fos.write(b, 0, read);
		                    total += read;
		                    r.setBar((int)Math.round(((double)total / (double)size) * 100f));
		                }
						
		                r.dispose();
		                
						System.out.println("Recipient: Done");
						fos.close();
						dos.close();
						dis.close();
						s.close();
						
						JOptionPane.showMessageDialog(run.frame, "File " + filename + " was received from " + ip);
						
						
					}
				} catch (IOException e) {
					r.dispose();
					e.printStackTrace();
				}
				
			}
		
		});
		t.run();
		
	}
}
