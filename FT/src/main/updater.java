package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class updater {

	int current = 0;
	
	public updater(){
		System.out.println("Checking for updates...");
		
		current = getCurrentVersion();
		
		if(current > run.release){
			System.out.println("Update found");
			JFrame frame = new JFrame();
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
					| UnsupportedLookAndFeelException e1) {
				e1.printStackTrace();
			}
			int select = JOptionPane.showConfirmDialog(frame, "Update found. Would you like to update now?");
			
			if(select == 0){
				try{
				URL website = new URL(run.download);
				ReadableByteChannel rbc = Channels.newChannel(website.openStream());
				FileOutputStream fos = new FileOutputStream(new File(System.getProperty("java.class.path")).getName());
				fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
				fos.close();
				System.exit(0);
				} catch(IOException ex){
					ex.printStackTrace();
					System.out.println("Update failed");
					JOptionPane.showMessageDialog(frame, "Update failed! Please try again later", "Error", 0);
				}
				JOptionPane.showMessageDialog(new JFrame(), "Updated FileTransfer!");
				frame.dispose();
				return;
			}
			System.out.println("Program update cancelled");
			return;
			
		}
		System.out.println("Program is up to date");
		
	}
	
	private int getCurrentVersion(){
		int re = 0;
		try {
			URLConnection con = new URL(run.currentVersion).openConnection();
			BufferedReader read = new BufferedReader(new InputStreamReader(con.getInputStream()));
			re = Integer.valueOf(read.readLine());
			read.close();
			con = null;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return re;
	}
	
}
