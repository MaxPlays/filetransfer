package main;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Color;
import javax.swing.SwingConstants;

public class mainframe extends JFrame {

	private static final long serialVersionUID = -7022406319993108517L;
	private JPanel contentPane;
	private JTextField ip;
	private JTextField file;
	
	static JButton push;
	
	static JProgressBar bar;
	
	public mainframe() {
		setResizable(false);
		setTitle("File Transfer");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 403, 223);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException | InstantiationException | IllegalAccessException
				| UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		ip = new JTextField();
		ip.setFont(new Font("Tahoma", Font.PLAIN, 25));
		ip.setBounds(12, 30, 368, 40);
		contentPane.add(ip);
		ip.setColumns(10);
		
		file = new JTextField();
		file.setEditable(false);
		file.setBounds(12, 93, 334, 22);
		contentPane.add(file);
		file.setColumns(10);
		
		JButton browse = new JButton("...");
		browse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			
				JFileChooser fc = new JFileChooser();
				fc.setDialogTitle("Select File");
                fc.setFileSelectionMode(0);
                fc.setApproveButtonText("Select");
                if(isWindows()){
                	Action details = fc.getActionMap().get("viewTypeDetails");
                	details.actionPerformed(null);
                }
                fc.setMultiSelectionEnabled(false);
                fc.showOpenDialog(null);
                
                if (fc.getSelectedFile() != null) {
                	file.setText(fc.getSelectedFile().getAbsolutePath());
                }
			
			}
		});
		browse.setBounds(350, 92, 30, 25);
		contentPane.add(browse);
		
		push = new JButton("Push to upstream");
		push.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				File f = new File(file.getText());
				if(!f.exists()){
					JOptionPane.showMessageDialog(run.frame, "File could not be found", "File Error", 0);
					return;
				}
				sender.sendFile(f, ip.getText());
			
			}
		});
		push.setBounds(100, 126, 180, 25);
		contentPane.add(push);
		
		bar = new JProgressBar();
		bar.setStringPainted(true);
		bar.setForeground(new Color(0, 128, 0));
		bar.setBounds(12, 164, 368, 26);
		bar.setVisible(false);
		contentPane.add(bar);
		
		JLabel lblDestination = new JLabel("Destination");
		lblDestination.setBounds(12, 13, 112, 16);
		contentPane.add(lblDestination);
		
		JLabel lblYourIp = new JLabel("Your IP:");
		lblYourIp.setBounds(12, 79, 46, 14);
		contentPane.add(lblYourIp);
		
		JLabel src = null;
		try {
			src = new JLabel(InetAddress.getLocalHost().getHostAddress());
			src.setHorizontalAlignment(SwingConstants.CENTER);
		} catch (UnknownHostException e1) {
			src = new JLabel("127.0.0.1");
			e1.printStackTrace();
		}
		src.setBounds(55, 79, 105, 14);
		contentPane.add(src);
		
		setVisible(true);
		
	}
	public void setBar(int value){
		bar.setValue(value);
	}
	public boolean isWindows(){
		return System.getProperty("os.name").toLowerCase().startsWith("win");
	}
}
